﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main()
        {
            for (int i = 0; i < 1000; i++)
            {
                //Use i when playing with it to see after how many iterations you get an out-of-memory exception.

                //1. In a using - so will call Dispose automatically after it's done
                var nonDisposable = new DisposesInUsing(i);

                //2. Calling the "ClassWithUnmanagedResource" outside of a using-block, so implements IDisposable itself.
                using (var a = new ImplementsIDisposable(i))
                {

                }

                //3. Incorrect:
                var notDisposingThis = new ImplementsIDisposable(i); //and "forget" to dispose

                //But not fatal because GC.collect is explicitly called. Comment that line to generate an out-of-memory exception.
                if (i % 100 == 0)
                {
                    GC.Collect(); //this is doing the finalization, so calls the destructor ~ImplementsIDisposable
                    Thread.Sleep(100); //sleep or else memory gets taken too quickly for GC to clean up (gc runs in a seperate thread).
                }
            }
        }
    }
}

public class DisposesInUsing
{
    public DisposesInUsing(int i)
    {
        using (var disposableObject = new ClassWithUnmanagedResource(i))
        {
            //do something
        }
    }
}

public class ImplementsIDisposable : IDisposable
{
    private ClassWithUnmanagedResource disposableObject;
    public ImplementsIDisposable(int i)
    {
        disposableObject = new ClassWithUnmanagedResource(i);
    }

    public void Dispose() //GOOD
    {
        disposableObject.Dispose();
//        GC.SuppressFinalize(this); //not needed when no finalizer
    }

//    ~ImplementsIDisposable() //NOT GOOD; 
//    {
//        disposableObject.Dispose(); //No need for this - disposableObject has its own destructor!
//    }
}

public class ClassWithUnmanagedResource:IDisposable
{
    private IntPtr unmanagedPointer;

    public ClassWithUnmanagedResource(int i)
    {
        unmanagedPointer = Marshal.AllocHGlobal(10 * 1024 * 1024); //Allocate 10 MB
    }

    private void FreeResources()
    {
        Marshal.FreeHGlobal(unmanagedPointer);
        unmanagedPointer = IntPtr.Zero;
    }

    public void Dispose()
    {
        FreeResources();
        GC.SuppressFinalize(this);
    }

    ~ClassWithUnmanagedResource()
    {
        FreeResources();
    }
}